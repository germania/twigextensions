<?php
namespace Germania\TwigExtensions;

use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigTest;
use Twig\TwigFilter;
use Germania\UrlPrefixer\UrlPrefixer;

class WebAppTwigExtension extends AbstractExtension implements GlobalsInterface
{
    protected $default_config = [
        'globals' => array(),
        'filters' => array(),
        'tests'   => array()
    ];

    public $globals = array();
    public $tests   = array();
    public $filters = array();


    /**
     * Accepts a two-dimensional array with filters and/or globals.
     * Example:
     *
     *     <?
     *     $ext = new GermaniaTwigExtension([
     *        'filters' => [ new Twig_SimpleFilter( ... ) ],
     *        'globals' => [ 'foo' => 'bar' ]
     *     ]);
     *
     * @param  array $config Optional
     * @uses   config()
     */
    public function __construct( $config = array())
    {
        // Add a test that checks if an URL is absolute.
        // Usage in Twig template:
        //
        //     {% if my_value is absolute_url %}
        //
        $this->addTest( new TwigTest('absolute_url', function ($url) {
            return preg_match("!(https?:)?\/\/!i", $url);
        }));


        // Usage in Twig template:
        //
        //    {{url|add_base_url( base_url )}}
        //
        // (base_url must be known by Twig, either as global or local template variable)
        $this->addFilter(new TwigFilter('add_base_url', new UrlPrefixer));


        // Handle config array, if given
        if ($config) {
            $this->config( $config );
        }

    }


    /**
     * Accepts a two-dimensional array with filters and/or globals.
     * Example:
     *
     *     <?
     *     $ext = new GermaniaTwigExtension;
     *     $ext->config([
     *        'filters' => [ new Twig_SimpleFilter( ... ) ],
     *        'tests'   => [ new Twig_SimpleTest( ...) ],
     *        'globals' => [ 'foo' => 'bar' ]
     *     ]);
     *
     * @param  array $config
     * @return self Fluid interface
     */
    protected function config( $config )
    {

        $settings = array_merge( $this->default_config, $config ?: array() );

        if (!empty($settings['globals'])
        and is_array($settings['globals'])) :
            foreach ($settings['globals'] as $name => $value)
                $this->addGlobal( $name, $value );
        endif;

        if (!empty($settings['filters'])
        and is_array($settings['filters'])) :
            foreach ($settings['filters'] as $filter )
                $this->addFilter( $filter );
        endif;

        if (!empty($settings['tests'])
        and is_array($settings['tests'])) :
            foreach ($settings['tests'] as $test )
                $this->addTest( $test );
        endif;

        return $this;
    }


    // ======================================================
    //
    // Convenience API
    //
    // ======================================================


    /**
     * Adds a global variable to Twig
     * @param  string $name  Name
     * @param  string $value Value
     * @return self Fluid interface
     */
    public function addGlobal($name, $value)
    {
        $this->globals[$name] = $value;
        return $this;
    }

    /**
     * Adds a TwigFilter instance
     * @param  TwigFilter $filter
     * @return self Fluid interface
     */
    public function addFilter( TwigFilter $filter )
    {
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * Adds a TwigTest instance
     * @param  TwigTest $test
     * @return self Fluid interface
     */
    public function addTest( TwigTest $test )
    {
        $this->tests[] = $test;
        return $this;
    }


    // ======================================================
    //
    // These methods override default Twig_Extension methods
    //
    // ======================================================


    /**
     * @return string "Germania"
     */
    public function getName()
    {
        return 'Germania';
    }

    /**
     * {@inheritdoc}
     *
     * @uses $globals
     *
     * @return array
     */
    public function getGlobals() : array
    {
        return $this->globals;
    }

    /**
     * {@inheritdoc}
     *
     * @uses $tests
     *
     * @return array
     */
    public function getTests() : array
    {
        return $this->tests;
    }

    /**
     * {@inheritdoc}
     *
     * @uses $filters
     *
     * @return array
     */
    public function getFilters() : array
    {
        return $this->filters;
    }
}
