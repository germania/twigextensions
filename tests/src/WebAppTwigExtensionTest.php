<?php
namespace tests;

use Germania\TwigExtensions\WebAppTwigExtension;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigTest;
use Twig\TwigFilter;

class WebAppTwigExtensionTest extends \PHPUnit\Framework\TestCase
{

    public function testComplianceToTwigExtensionRules() {
        $sut = new WebAppTwigExtension;

        $this->assertInstanceOf( AbstractExtension::class, $sut );

        $this->assertTrue( is_array( $sut->getGlobals()) );
        $this->assertTrue( is_array( $sut->getTests()) );
        $this->assertTrue( is_array( $sut->getFilters()) );
        $this->assertTrue( is_string( $sut->getName()) );
    }



    public function testDefaultBuiltInTests() {
        $sut = new WebAppTwigExtension;

        $tests = $sut->getTests();
        $absolute_url = $tests[ 0 ];
        $this->assertInstanceOf( TwigTest::class, $absolute_url );
        $this->assertEquals( "absolute_url", $absolute_url->getName() );
    }



    public function testDefaultBuiltInFilters() {
        $sut = new WebAppTwigExtension;

        $filters = $sut->getFilters();
        $add_base_url = $filters[ 0 ];
        $this->assertInstanceOf( TwigFilter::class, $add_base_url );
        $this->assertEquals( "add_base_url", $add_base_url->getName() );
    }





    /**
     * @dataProvider provideGlobals
     */
    public function testGlobals( $var, $value)
    {
        $sut1 = new WebAppTwigExtension;
        $sut1->addGlobal($var, $value);

        $globals = $sut1->getGlobals();
        $this->assertArrayHasKey( $var, $globals );
        $this->assertEquals( $value, $globals[ $var ]);

        // Test with Configuration on instantiation
        $sut2 = new WebAppTwigExtension([
            'globals' => [ $var => $value ]
        ]);
        $globals2 = $sut2->getGlobals();
        $this->assertArrayHasKey( $var, $globals2 );
        $this->assertEquals( $value, $globals2[ $var ]);

    }


    /**
     * @dataProvider provideSimpleTests
     */
    public function testSimpleTests( $test)
    {
        $sut1 = new WebAppTwigExtension;
        $sut1->addTest($test);

        $tests = $sut1->getTests();
        $this->assertContains( $test, $tests );

        // Test with Configuration on instantiation
        $sut2 = new WebAppTwigExtension([
            'tests' => [ $test ]
        ]);
        $this->assertContains( $test, $sut2->getTests() );
    }


    /**
     * @dataProvider provideSimpleFilters
     */
    public function testSimpleFilters( $filter)
    {
        $sut1 = new WebAppTwigExtension;
        $sut1->addFilter($filter);

        $filters = $sut1->getFilters();
        $this->assertContains( $filter, $filters );

        // Test with Configuration on instantiation
        $sut2 = new WebAppTwigExtension([
            'filters' => [ $filter ]
        ]);
        $this->assertContains( $filter, $sut2->getFilters() );
    }





    public function provideSimpleTests()
    {
        return array(
            [ new TwigTest('bar'), function() {} ]
        );
    }

    public function provideSimpleFilters()
    {
        return array(
            [ new TwigFilter('foo', function() {} ) ]
        );
    }

    public function provideGlobals()
    {
        return array(
            [ 'foo',  'bar' ],
            [ 'foo2', 'bar1' ]
        );
    }
}
